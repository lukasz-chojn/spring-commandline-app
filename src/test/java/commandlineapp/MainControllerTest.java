package commandlineapp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainControllerTest {

    @Test
    void run() {
        String[] strings = {"test 1", "test 2", "test 3"};

        assertNotNull(strings);
        assertEquals(3, strings.length);
    }
}