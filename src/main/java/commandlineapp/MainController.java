package commandlineapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MainController implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        for (int i = 0; i < args.length; ++i) {
            System.out.println("Argument przekazany z CLI to: " + args[i]);
        }
    }
}